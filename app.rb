require 'sinatra'

MOVIES = [
  { id: 'q1', name: 'ビーチ', choice: false, comment: 'うーん' },
  { id: 'q2', name: 'アビエイター', choice: false, comment: '残念やったな' },
]

get '/' do
  @movies = MOVIES
  erb :index
end

get '/check' do
  @results = []
  MOVIES.each do |movie|
    result = {}
    result[:name] = movie[:name]
    result[:watched] = params[movie[:id]] == 'はい' ? true : false
    result[:comment] = movie[:comment]
    @results << result
  end
  erb :check
end
